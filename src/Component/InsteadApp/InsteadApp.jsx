import React from 'react';
// import * as React from "react"
import '../../App.css';
import Modal from "../Modal/Modal";
import ProductList from "../ProductList/ProductList";

class InsteadApp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cart: {},
            favorite: {},

            carToCart: '',

            displayModalId: '',

            buttons: {
                btnSubmit: {
                    backgroundColor: 'green',
                    text: 'Submit',
                    onClick: () => {
                        console.log('submit')
                    }
                },
                btnCancel: {
                    backgroundColor: '#CCCC00',
                    text: 'Cancel',
                    onClick: this.onCancel
                }
            },

            modals: {
                firstMod: {
                    header: 'Add to cart',
                    closeButton: true,
                    text: 'Do you want to add it to cart?',
                },
                secondMod: {
                    header: 'Remove from cart',
                    closeButton: true,
                    text: 'Are you sure to remove item from cart?',
                }
            },

            items: [],
        };
    }

    componentDidMount() {
        const localStorageRef = localStorage.getItem(JSON.stringify("cart"))
        const localStorageFavor = localStorage.getItem(JSON.stringify("favorite"))
        if (localStorageRef) {
            this.setState({cart: JSON.parse(localStorageRef)})
        }

        if (localStorageFavor) {
            this.setState({favorite: JSON.parse(localStorageFavor)})
        }

        fetch('/products.json')
            .then(response => response.json())
            .then(data => {
                this.setState({products: data});
            })
            .catch(err => console.error((err)));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem(JSON.stringify("cart"), JSON.stringify(this.state.cart))
        localStorage.setItem(JSON.stringify("favorite"), JSON.stringify(this.state.favorite))
    }

    addToCart = (displayModalId) => {
        const key = this.state.carToCart
        const cart = {...this.state.cart}
        if(displayModalId === 'first'){
            cart[key] = cart[key] + 1 || 1;
        } else if(displayModalId === 'second'){
            cart[key] = 0;
        }
        this.setState({cart: cart});
        this.setState({carToCart: ''})
        this.setState({displayModalId: ''})
    }

    addToFavorite = (index) => {
        const favorite = {...this.state.favorite}
        favorite[index] = !favorite[index];

        this.setState({favorite: favorite})
    }

    onCancel = () => {
        this.setState({displayModalId: ''})
    }

    setActive = () => {
        this.setState({displayModalId: ''})
    }

    showModal = (mod, articles) => {
        if (mod === 'first') {
            this.setState({carToCart: articles})
        } else if (mod === 'second') {
            this.setState({carToCart: articles})
        }
        this.setState({displayModalId: mod})
    }


    render() {
        if (!this.state.products) {
            return <p>loading</p>
        }

        const { path } = this.props

        const {
            displayModalId,
            modals,
            buttons,
            products,
            favorite,
            cart,
        } = this.state;

        let list = [];

        if(!path){
            list = products
        } else if (path === 'favorite'){
            list = products.filter((item, index) => favorite[item.articles])
        } else if (path === 'cart'){
            list = products.filter((item, index) => (cart[item.articles]>0))
        }

        return (
            <div className="App">
                <ProductList
                    list={list}
                    buttons={buttons}
                    addToCart={this.addToCart}
                    favorite={favorite}
                    addToFavorite={this.addToFavorite}
                    showModal={this.showModal}
                    path={path}
                />
                <Modal setActive={this.setActive}
                       modals={modals}
                       displayModalId={displayModalId}
                       buttons={buttons}
                       addToCart={this.addToCart}
                />
                {/*<Btn {...buttons.btnCancel}/>*/}
                {/*<button onClick={() => this.showModal('first')} className="btnModal">Show first modal</button>*/}
                {/*<button onClick={() => this.showModal('second')} className="btnModal">Show second modal</button>*/}
            </div>
        );
    }

}

export default InsteadApp;