import React from 'react';
import {NavLink} from "react-router-dom";
import './NavBar.scss'

const NavBar = () => {
    return (
        <ul>
            <li><NavLink exact
                         className={'navbar__link'}
                         activeClassName={'navbar__link--active'}
                         to={'/'}>Home</NavLink></li>
            <li><NavLink exact
                         className={'navbar__link'}
                         activeClassName={'navbar__link--active'}
                         to={'/favorite'}>Favorite</NavLink></li>
            <li><NavLink exact
                         className={'navbar__link'}
                         activeClassName={'navbar__link--active'}
                         to={'/cart'}>Cart</NavLink></li>
        </ul>
    );
};

export default NavBar;