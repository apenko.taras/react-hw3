import React from 'react';
import Btn from "../Btn/Btn";
import './Modal.scss';
import CloseIcon from '@material-ui/icons/Close';

function Modal({displayModalId, modals, buttons, setActive, addToCart}) {

    const handleClickSub = () => {
        if(displayModalId === 'first'){
            addToCart(displayModalId)
        } else if (displayModalId === 'second'){
            addToCart(displayModalId)
        }
    }

    const chooseModal = (displayModalId, modals) => {
        if (displayModalId === 'first') {
            return modals.firstMod;
        } else if (displayModalId === 'second') {
            return modals.secondMod;
        }
    }

    const svgClose = () => {
        if (modalInfo.closeButton) {
            return (
                <div onClick={setActive}>
                    <CloseIcon name={'close'} fontSize='large' color={'red'}/>
                </div>
            )
        }
    }

    const modalInfo = displayModalId ? chooseModal(displayModalId, modals) : '';

    const modal =
        <div className="modalOverlay" onClick={setActive}>
            <div className="modalWindow" onClick={event => event.stopPropagation()}>
                <div className="modalHeader">
                    <p className='modalHeader__text'>{modalInfo.header}</p>
                    {svgClose()}
                </div>
                <div className="modalBody">
                    <p>{modalInfo.text}</p>
                </div>
                <div className="modalFooter">
                    <Btn {...buttons.btnSubmit} onClick={handleClickSub}/>
                    <Btn {...buttons.btnCancel}/>
                </div>
            </div>
        </div>
    const ifModal = displayModalId ? modal : '';
    return (
        ifModal
    )

}

export default Modal