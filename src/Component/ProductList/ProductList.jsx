import React from 'react';
import PropTypes from 'prop-types';
import Product from "../Product/Product";
import "./ProductList.scss"


function ProductList({ list, buttons, addToCart, favorite, addToFavorite, showModal, path }) {

    const renderProdCard = () => {
        return list.map((product, index) => (
            <Product
                key={product.articles}
                {...product}
                // buttons={buttons}
                // addToCart={addToCart}
                index={product.articles}
                favorite={favorite}
                addToFavorite={addToFavorite}
                showModal={showModal}
                articles={product.articles}
                path={path}
            />
        ));
    }

    return (
        <div className="products">{renderProdCard()}</div>
    )

}

// ProductList.propTypes = {
//     list: PropTypes.arrayOf(
//         PropTypes.shape({
//             from: PropTypes.string,
//             to: PropTypes.string,
//             text: PropTypes.string
//         })
//     )
// };

export default ProductList;